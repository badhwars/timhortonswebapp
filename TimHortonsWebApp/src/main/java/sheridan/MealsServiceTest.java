package sheridan;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class MealsServiceTest {

	
	@Test
	public void testDrinksRegular() {
		MealsService mealService = new MealsService();
		List<String> mealList = mealService.getAvailableMealTypes(MealType.DRINKS);
		int size = mealList.size();
		assertTrue(size!=0);
		
	}
	
	@Test
	public void testDrinksException() {
		MealsService mealService = new MealsService();
		List<String> types = mealService.getAvailableMealTypes(null);
		assertTrue(types.get(0).equals("No Brand Available"));
	}
	@Test
	public void testDrinksBoundaryIn() {
		MealsService mealService = new MealsService();
		List<String> types = mealService.getAvailableMealTypes(MealType.DRINKS);
		int size = types.size();
		assertTrue(size > 3);
	}
	@Test
	public void testDrinksBoundaryOut() {
		MealsService mealService = new MealsService();
		List<String> types = mealService.getAvailableMealTypes(null);
		int size = types.size();
		assertTrue(size<=1);
	}

}
