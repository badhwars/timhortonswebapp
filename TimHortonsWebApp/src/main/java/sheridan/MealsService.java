package sheridan;

import sheridan.MealType;

import java.util.ArrayList;
import java.util.List;

public class MealsService {
	

    public List<String> getAvailableMealTypes( MealType type ){

        List<String> types = new ArrayList<String>( );

        if( type != null && type.equals( MealType.DRINKS ) ){
          types.add( "Coffee" );
          types.add( "Ice Cap" );
          types.add( "Tea" );
          types.add( "Water" );
          
        }
        else if( type != null && type.equals( MealType.BAKEDGOODS ) ){
            types.add( "Muffin" );
            types.add( "Doughnuts" );
        }
        else {
        	types.add("No Brand Available");
        }
        return types;
    }
}
